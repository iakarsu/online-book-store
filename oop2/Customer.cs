﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OOP
{
    class Customer
    {
        private string customerID { get; set; }
        private string name { get; set; }
        private string adress { get; set; }
        private string email { get; set; }
        private string username { get; set; }
        private string password { get; set; }

        public Customer(string customerID, string name, string adress, string email, string username, string password)
        {
            this.customerID = customerID;
            this.name = name;
            this.adress = adress;
            this.email = email;
            this.username = username;
            this.password = password;
        }

        //returns an array of informations of customer
        public string[] printCustomerDetails()
        {
            List<string> customer = new List<string>();
            customer.Add(this.customerID);
            customer.Add(this.name);
            customer.Add(this.adress);
            customer.Add(this.email);
            customer.Add(this.username);
            customer.Add(this.password);

            string[] customerArray = customer.ToArray();
            return customerArray;
        }

        public void saveCustomer()
        {
            //update işlemi için, formdan gelen bilgiler
        }

        public void printCustomerPurchases()
        {
            //databaseden  verileri çekip göster
        }
    }
}
