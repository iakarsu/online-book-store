﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OOP
{
    class ItemToPurchase
    {
        ShoppingCart cart = new ShoppingCart();
        private string productID; //id ye göre özellikleri db'den çekip shopping carta eklicez.
        private string quantity;

        public ItemToPurchase(string productID, string quantity)
        {
            this.productID = productID;
            this.quantity = quantity;
        }

        public void sendItemToCart()
        {
            cart.addProduct(this.productID, this.quantity);
        }
    }
}
