﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OOP
{
    class ShoppingCart
    {
        private string customerID { get; set; }
        private Dictionary<string, string> itemsToPurchase = new Dictionary<string, string>();
        private string paymentAccount { get; set; }
        private string paymentType { get; set; }

        

        public ShoppingCart()
        {

        }

        public void printProducts()
        {
            //formda market sepetine tıklayınca bu fonksiyon çalışacak
        }

        public void addProduct(string id, string quantity)
        {
            itemsToPurchase.Add(id,quantity);
        }

        public void removeProduct(string id)
        {
            itemsToPurchase.Remove(id);
        }

        public void placeOrder()
        {
            //ekrana ürünler özellikleri ve toplam fiyatı yazdır
        }

        public void cancelOrder()
        {

        }

        public void sendInvoidcebyEmail()
        {

        }
    }
}
